#AIP Payment Request

Version: **1.0**

Esta libreria le ayudará a utilizar el Gateway de Pago de AIP a traves de la plataforma Android

####Pasos para efectuar un pago a traves del gateway

1-Generar un nonce del cliente para autenticacion, luego con ese nonce, genera un token a traves de login, esta validacion no puede ser del lado del cliente (web, android y ios), debe ser realizado a traves de un webservice para fines de seguridad y ser transparente para el lado del cliente

2- Generar un token para pagos, genera el intent con la clase "PaymentRequest" colocandole de parametro el nonce de autenticación e iniciar la actividad en android. Se abrirá una ventana en la que el cliente validará los datos de pago.

3- Crear una transaccion.

4- Si el servicio a cobrar fue efectuado correctamente, se hace una confirmacion de la transacción, en caso de que haya algun error en el servicio a ofrecer, se hace una anulacion.

Ejemplo: 

Si se procede a hacer una recarga de un servicio de topup en una aplicacion movil:

Se genera el nonce del cliente y login para obtener el token de autenticacion y el token de pago (lado de webservice), con ese token, generará el intent a presentar. Una vez que el cliente termine de completar sus datos, se le mostrará una pantalla de confirmación (Aplicacion Movil). La aplicacion movil hace la confirmacion, se procede haciendo la compra creando la transaccion y se efectua la recarga, si la recarga es exitosa, se confirma la transaccion, si falla, se procede a hacer una anulación.


   
