package com.aip.paymentGateway.android;

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.message.BasicHeader;

/**
 * Class for testing the payment gateway from AIP.
 * Note: it was create only for testing purpose, this logic must be in a web service for security
 * Created by fernandomanuelperezramos on 9/24/16.
 */
public class PaymentGatewayTest {

    public static final String LOG_TAG = PaymentGatewayTest.class.getCanonicalName();

    public static final String DEFAULT_HOST_PROD = "https://payments.aiplatform.net/";
    public static final String DEFAULT_HOST_DEV  = "https://payments-dev.aiplatform.net/";
    public static final String DEFAULT_USER_PROD = "devtest@local";
    public static final String DEFAULT_USER_DEV = "devtest@local";
    public static final String DEFAULT_PASS_PROD = "fk49df33jsSdjW$$";
    public static final String DEFAULT_PASS_DEV = "fk49df33jsSdjW$$";

    /**
     * Host name of webserver
     */
    private String ws_host;

    /**
     * User to access the payment gateway
     */
    private String ws_user;

    /**
     * Password to access the payment gateway
     */
    private String ws_password;

    /**
     * Nonce of authentication
     */
    private String nonce;

    /**
     * Token of the current session
     */
    private String token;

    /**
     * Method to contain the context of the activity
     */
    private Context context;


    public interface OnRequestTokenListener{
        void onSuccess(String token);
        void onError();
    }

    interface OnRequestNonceListener{
        void onSuccess(String nonce);
        void onError();
    }

    public interface OnRequestPaymentListener{
        void onSuccess(String transactionId);
        void onCompleted();
        void onError();
    }

    public interface OnRequestConfirmTransaction{
        void onSuccess(JSONObject transaction);
        void onCompleted();
        void onError();
    }

    public interface OnRequestVoidTransaction{
        void onSuccess(JSONObject transaction);
        void onCompleted();
        void onError();
    }

    public PaymentGatewayTest(Context context){
        //TODO: replace this url
        this.ws_host        = DEFAULT_HOST_DEV;
        this.ws_user        = DEFAULT_USER_DEV;
        this.ws_password    = DEFAULT_PASS_DEV;
        this.context = context;
    }

    /**
     * Method to obtain the url webservice
     */
    private String getWSUrl(String path){
        return String.format(Locale.getDefault(),"%s%s",this.ws_host,path);
    }

    private void getNonce(final OnRequestNonceListener listener){
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(getWSUrl("api/auth/nonce"),new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                String nonce = null;

                try{
                    if (response != null && response.has("nonce") && response.getString("nonce") != null)
                        nonce = response.getString("nonce");
                }catch (Exception exc){
                    nonce = null;
                    Log.e(LOG_TAG,exc.getMessage());
                }
                if (nonce != null) {
                    PaymentGatewayTest.this.nonce = nonce;
                    listener.onSuccess(nonce);
                }
                else listener.onError();
            }
        });
    }

    /**
     * Method to convert an string to sha256
     */
    private String sha256(String str){
        String sha256 = null;
        MessageDigest digest=null;
        byte[] byteDigest;

        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        }
        if (digest != null){
            digest.reset();
            byteDigest = digest.digest(str.getBytes());
            StringBuilder sb = new StringBuilder(byteDigest.length * 2);

            Formatter formatter = new Formatter(sb);
            for (byte b : byteDigest) {
                formatter.format("%02x", b);
            }
            sha256 = sb.toString();
        }
        return sha256;
    }

    /**
     * Method to generate the authentication token
     * structure: SHA256(nonce + ':' + SHA256(email + ':' + password))
     */
    private String generateDigest(String nonce){
        String digestFormat = "%s:%s";
        String digest = sha256(String.format(Locale.getDefault(),digestFormat,this.ws_user,this.ws_password));
        digest = sha256(String.format(Locale.getDefault(),digestFormat,nonce,digest));
        return digest;
    }


    /**
     * Method to make a payment
     * Note: this is created for testing purpose only, this logic must be in a web service for security
     */
    public void makePayment(String nonce,Double amount,String customerName,String customerNumber,final OnRequestPaymentListener listener){
        if (this.token == null){
            Log.e(LOG_TAG,"the token of the authentication is null or invalid");
            listener.onError();
        }

        AsyncHttpClient makePaymentRequest = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("payment_method_nonce",nonce);
        params.put("amount",amount);

        makePaymentRequest.post(context,getWSUrl("api/payments/pay"),new Header[]{
                new BasicHeader("x-access-token",this.token)
        },params,"application/x-www-form-urlencoded",new JsonHttpResponseHandler(){

            @Override
            public void onFinish() {
                super.onFinish();
                listener.onCompleted();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                listener.onError();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                String transactionId = null;

                try{
                    transactionId = response.has("transactionId") && response.has("status") && response.getString("status").equals("pending") ? response.getString("transactionId") : null;
                }catch (Exception exc){
                    Log.e(LOG_TAG,"error in \"makePayment\" method",exc);
                }
                //{"transactionId":"66b90a65-f415-4cf5-b62c-6551771afa62","status":"pending","amount":29.99,"createdAt":"2016-10-05T23:10:55.447Z"}
                if (transactionId == null) listener.onError();
                else listener.onSuccess(transactionId);
            }
        });
    }


    /**
     * Method to confirm the transaction
     * Note: this is created for testing purpose only, this logic must be in a web service for security
     */
    public void confirmTransaction(String transactionId,final OnRequestConfirmTransaction listener){
        AsyncHttpClient confirmTransactionRequest = new AsyncHttpClient();

        RequestParams params = new RequestParams();
        params.put("transactionId",transactionId);

        confirmTransactionRequest.post(context,getWSUrl("api/payments/confirm"),new Header[]{
                new BasicHeader("x-access-token",token)
        },params,"application/x-www-form-urlencoded",new JsonHttpResponseHandler(){

            @Override
            public void onFinish() {
                super.onFinish();
                listener.onCompleted();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                listener.onError();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                //{"transactionId":"98bfd8db-3921-47d9-a2e1-58ee171f6a4f","status":"processed","amount":29.99,"createdAt":"2016-10-06T00:03:58.385Z"}
                JSONObject output = null;

                try{
                    if (response.has("transactionId") && response.has("status") && response.getString("status").equals("processed"))
                        output = response;
                }catch(Exception exc){
                    Log.e(LOG_TAG,"error in \"confirmTransaction\" method",exc);
                }

                if (output == null)
                    listener.onError();
                else listener.onSuccess(output);
            }
        });
    }

    /**
     * Method to make void of a transaction
     * Note: this is created for testing purpose only, this logic must be in a web service for security
     */
    public void voidTransaction(String transactionId,final OnRequestVoidTransaction listener){
        AsyncHttpClient voidTransactionRequest = new AsyncHttpClient();

        RequestParams params = new RequestParams();
        params.put("transactionId",transactionId);

        voidTransactionRequest.post(context,getWSUrl("api/payments/void"),new Header[]{
                new BasicHeader("x-access-token",token)
        },params,"application/x-www-form-urlencoded",new JsonHttpResponseHandler(){

            @Override
            public void onFinish() {
                super.onFinish();
                listener.onCompleted();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                listener.onError();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                //{"transactionId":"98bfd8db-3921-47d9-a2e1-58ee171f6a4f","status":"processed","amount":29.99,"createdAt":"2016-10-06T00:03:58.385Z"}
                JSONObject output = null;

                try{
                    if (response.has("transactionId") && response.has("status") && response.getString("status").equals("voided"))
                        output = response;
                }catch(Exception exc){
                    Log.e(LOG_TAG,"error in \"voidTransaction\" method",exc);
                }
                listener.onSuccess(response);
            }
        });
    }

    /**
     * Method to obtain the token for payment
     * Note: this is created for testing purpose only, this logic must be in a web service for security
     */
    public void getToken(final OnRequestTokenListener listener){
        getNonce(new OnRequestNonceListener() {
            @Override
            public void onSuccess(String nonce) {
                AsyncHttpClient loginRequest = new AsyncHttpClient();

                String digest = generateDigest(nonce);
                RequestParams params = new RequestParams();
                params.put("nonce",nonce);
                params.put("email",ws_user);
                params.put("digest",digest);
                loginRequest.post(context,getWSUrl("api/auth/login"),params,new JsonHttpResponseHandler(){
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        //{"token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1N2U1M2E2YzJjMDQyODY5MTg4ZDAzYTEiLCJlbWFpbCI6ImRldnRlc3RAbG9jYWwiLCJuYW1lIjoiRGV2ZWxvcGVyIFRlc3QiLCJjcmVhdGVkQXQiOiIyMDE2LTEwLTAxVDE2OjU0OjMyLjM4MFoiLCJhcHBzIjpbXSwiZW5hYmxlZCI6dHJ1ZSwiaWF0IjoxNDc1MzQwODcyLCJleHAiOjE0NzUzNDQ0NzIsImlzcyI6IkFJUCJ9.chwajmrr3fLZN51_BWDw3AOuIxNbcbBiVYZ_u3-v9zc"}
                        String token = null;
                        try{
                            token = response.has("token") ? response.getString("token") : null;
                        }catch (Exception exc){
                            Log.e(LOG_TAG,"error en \"getToken\" method",exc);
                        }

                        if (token == null)
                            listener.onError();
                        else {
                            PaymentGatewayTest.this.token = token;

                            //retrieve payment token
                            AsyncHttpClient tokenPaymentRequest = new AsyncHttpClient();

                            tokenPaymentRequest.get(context,getWSUrl("api/payments/token"),new Header[]{
                                    new BasicHeader("x-access-token",token)
                            },new RequestParams(),new JsonHttpResponseHandler(){
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                    super.onSuccess(statusCode, headers, response);
                                    String paymentToken;
                                    try{
                                        paymentToken = response == null || !response.has("token") ? null : response.getString("token");
                                    }catch (Exception exc){
                                        Log.e(LOG_TAG,"error  requiring the token payment",exc);
                                        paymentToken = null;
                                    }

                                    if (paymentToken == null){
                                        listener.onError();
                                        return;
                                    }
                                    listener.onSuccess(paymentToken);
                                }

                                @Override
                                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                                    super.onFailure(statusCode, headers, responseString, throwable);
                                    listener.onError();
                                }
                            });
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        listener.onError();
                    }
                });
            }

            @Override
            public void onError() {
                listener.onError();
            }
        });
    }
}
