package com.aip.paymentGateway.android;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;

import com.braintreepayments.api.BraintreePaymentCustomActivity;
import com.braintreepayments.api.PayPal;
import com.braintreepayments.api.PaymentRequest;

import java.util.List;
import java.util.Locale;

/**
 * Class for handle the payment activity (drop-in)
 * Created by fernandomanuelperezramos on 10/6/16.
 */
public class AIPPaymentRequest {

    public static final class RESULTS{
        /** Standard activity result: operation canceled. */
        public static final int CANCELED = 0;

        public static final int OK = -1;
    }

    /**
     * Parceable to generate the intent of the activity of payment
     */
    private PaymentRequest paymentRequest;

    /**
     * Constructor
     */
    public AIPPaymentRequest(){
        this.paymentRequest = new PaymentRequest();
    }

    /**
     * Method to set the tokenization key generated from AIP Payment Gateway Rest Service
     *
     * This Method is optional
     */
    public AIPPaymentRequest tokenizationKey(String tokenizationKey) {
        this.paymentRequest.tokenizationKey(tokenizationKey);
        return this;
    }

    /**
     * Method to set the client token generate from AIP Payment Gateway Rest Service
     *
     * This Method is optional
     */
    public AIPPaymentRequest clientToken(String clientToken){
        this.paymentRequest.clientToken(clientToken);
        return this;
    }

    /**
     * Method to set the title to show inside the activity
     */
    public AIPPaymentRequest setTitle(String title){
        this.paymentRequest.primaryDescription(title);
        return this;
    }

    /**
     * Method to set the subtitle to show inside the activity
     */
    public AIPPaymentRequest setSubtitle(String subtitle){
        this.paymentRequest.secondaryDescription(subtitle);
        return this;
    }


    /**
     *
     * Method to set the title to show inside the activity
     *
     * This Method is optional
     *
     */
    public AIPPaymentRequest setAmount(Double amount){
        this.setAmount(String.format(Locale.getDefault(),"$%.2f",amount));
        return this;
    }

    /**
     * Method to set the title to show inside the activity
     *
     * This Method is optional
     */
    public AIPPaymentRequest setAmount(String amount){
        this.paymentRequest.amount(amount);
        return this;
    }

    /**
     * Method to obtain the intent to start the activity
     */
    public Intent getIntent(Context context) {
        return this.paymentRequest.getIntent(context);
    }


    /**
     * Method to disable the paypal function
     */
    public AIPPaymentRequest disablePayPal(){
        this.paymentRequest.disablePayPal();
        return this;
    }

    /**
     * Method to disable the android pay function
     */
    public AIPPaymentRequest disableAndroidPay(){
        this.paymentRequest.disableAndroidPay();
        return this;
    }

    /**
     * Method to disable the venmo function
     */
    public AIPPaymentRequest disableVenmo(){
        this.paymentRequest.disableVenmo();
        return this;
    }

    /**
     * Method to add the scopes of paypal
     * Ex: PayPal.SCOPE_ADDRESS.
     *        Acceptable scopes are defined in {@link PayPal}.
     */
    public AIPPaymentRequest paypalAdditionalScopes(List<String> scopes){
        this.paymentRequest.paypalAdditionalScopes(scopes);
        return this;
    }

    /**
     * Method to obtain the extra value to put in the activity
     */
    public Parcelable getExtra(){
        return this.paymentRequest;
    }
}
