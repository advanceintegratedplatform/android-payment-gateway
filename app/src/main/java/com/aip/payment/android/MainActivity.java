package com.aip.payment.android;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.aip.paymentGateway.android.AIPPay;
import com.aip.paymentGateway.android.AIPPayMethodNonce;
import com.aip.paymentGateway.android.AIPPaymentRequest;
import com.aip.paymentGateway.android.PaymentGatewayTest;

import org.json.JSONObject;

import java.util.Locale;

public class MainActivity extends AppCompatActivity{

    public static class Product{
        public String description;
        public Double price;

        public String getPriceFormatted(){
            if (price == null) return null;
            return String.format(Locale.getDefault(),"$%.2f",this.price);
        }

    }

    private static final int DROP_IN_REQUEST = 100;


    PaymentGatewayTest paymentGateway;


    private Product mProduct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mProduct = new Product();
        mProduct.description = "Bolitas Adivinadoras";
        mProduct.price = 29.99;


        TextView txLblProductDescription = (TextView) findViewById(R.id.lblProductDescription);
        if (txLblProductDescription != null){
            txLblProductDescription.setText(mProduct.description);
        }

        TextView txLblProductPrice= (TextView) findViewById(R.id.lblProductPrice);
        if (txLblProductPrice != null){
            txLblProductPrice.setText(mProduct.getPriceFormatted());
        }
    }

    public void buyProduct(View v){
        final ProgressDialog pDialog = ProgressDialog.show(this,getString(R.string.loading),getString(R.string.loadingPaymentService));
        pDialog.setCancelable(false);

        paymentGateway = new PaymentGatewayTest(this);
        paymentGateway.getToken(new PaymentGatewayTest.OnRequestTokenListener() {
            @Override
            public void onSuccess(final String token) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pDialog.dismiss();

                        AIPPaymentRequest paymentActivity = new AIPPaymentRequest()
                                .clientToken(token)
                                .setTitle(getString(R.string.cart))
                                .setSubtitle(mProduct.description)
                                .setAmount(mProduct.getPriceFormatted());


                        Intent i  = paymentActivity.getIntent(MainActivity.this);
                        i.putExtra(AIPPay.EXTRA_CHECKOUT_REQUEST, paymentActivity.getExtra());
                        startActivityForResult(i,DROP_IN_REQUEST);
                    }
                });
            }

            @Override
            public void onError() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pDialog.dismiss();
                    }
                });
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == DROP_IN_REQUEST) {
            switch (resultCode) {
                case AIPPaymentRequest.RESULTS.OK:
                    AIPPayMethodNonce payMethodNonce = new AIPPayMethodNonce(data.getParcelableExtra(AIPPay.EXTRA_PAYMENT_METHOD_NONCE));
                    String nonce = payMethodNonce.getNonce();
                    showConfirmView(nonce);
                    break;
                case AIPPay.RESULT_DEVELOPER_ERROR:
                case AIPPay.RESULT_SERVER_ERROR:
                case AIPPay.RESULT_SERVER_UNAVAILABLE:
                    // handle errors here, a throwable may be available in
                    // data.getSerializableExtra(BraintreePaymentActivity.EXTRA_ERROR_MESSAGE)
                    Log.e(MainActivity.class.getCanonicalName(),"error: " + data.getSerializableExtra(AIPPay.EXTRA_ERROR_MESSAGE));
                    break;
                default:
                    break;
            }
        }
    }

    private void showConfirmView(String nonce){
        final ProgressDialog pDialog = ProgressDialog.show(MainActivity.this,getString(R.string.loading),getString(R.string.creating_transaction));

        paymentGateway.makePayment(nonce, mProduct.price, "", "", new PaymentGatewayTest.OnRequestPaymentListener() {
            @Override
            public void onSuccess(final String transactionId) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        final Dialog dialog = new Dialog(MainActivity.this);
                        dialog.setTitle(R.string.confirmPayment);
                        dialog.setContentView(R.layout.dg_confirm_payment);

                        TextView txProductPrice = (TextView) dialog.findViewById(R.id.lblProductPrice);
                        if (txProductPrice != null) txProductPrice.setText(mProduct.getPriceFormatted());

                        TextView txProductDescription = (TextView) dialog.findViewById(R.id.lblProductDescription);
                        if (txProductDescription != null) txProductDescription.setText(mProduct.description);

                        Button btnConfirmPayment = (Button) dialog.findViewById(R.id.btnConfirmPayment);
                        if (btnConfirmPayment != null){
                            btnConfirmPayment.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    confirmTransaction(transactionId,v);
                                }
                            });
                        }

                        Button btnVoidPayment = (Button) dialog.findViewById(R.id.btnVoidPayment);
                        if (btnVoidPayment != null){
                            btnVoidPayment.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    voidTransaction(transactionId,v);
                                }
                            });
                        }
                        dialog.setCancelable(false);
                        dialog.show();
                    }
                });
            }

            @Override
            public void onCompleted() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pDialog.dismiss();
                    }
                });

            }

            @Override
            public void onError() {
                Toast.makeText(MainActivity.this,R.string.msg_errorCreatingPayment,Toast.LENGTH_LONG).show();
            }
        });
        Log.d(MainActivity.class.getCanonicalName(),"nonce: " + nonce);
    }

    /**
     * Method to confirm the transaction
     */
    public void confirmTransaction(String transactionId,View v){
        final ProgressDialog pDialogConfirm = ProgressDialog.show(MainActivity.this,getString(R.string.loading),getString(R.string.confirming_transaction));
        //confirm the payment, here you can make the "buy" method of the web service, that will confirm the sales and will return data of the transaction
        paymentGateway.confirmTransaction(transactionId, new PaymentGatewayTest.OnRequestConfirmTransaction() {
            @Override
            public void onSuccess(final JSONObject transaction) {
                //<!--{"transactionId":"98bfd8db-3921-47d9-a2e1-58ee171f6a4f","status":"processed","amount":29.99,"createdAt":"2016-10-06T00:03:58.385Z"}-->
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String transactionId = null,status = null,amount = null, dateTransaction = null;
                        try{
                            transactionId = "123123";
                            status = transaction.has("status") ? transaction.getString("status") : null;
                            amount = transaction.has("amount") ? transaction.getString("amount") : null;
                            dateTransaction = transaction.has("createdAt") ? transaction.getString("createdAt") : null;
                        }catch (Exception exc){
                            Log.e(MainActivity.class.getCanonicalName(),"",exc);
                        }

                        final Dialog dialogVoucher = new Dialog(MainActivity.this);
                        dialogVoucher.setTitle(R.string.thanksForYourPurchase);
                        dialogVoucher.setContentView(R.layout.dg_voucher_payment);
                        dialogVoucher.setCancelable(false);

                        TextView txDate = (TextView) dialogVoucher.findViewById(R.id.lblDate);
                        if (txDate != null) txDate.setText(dateTransaction);

                        TextView txTransaction = (TextView) dialogVoucher.findViewById(R.id.lblTransactionId);
                        if (txTransaction != null) txTransaction.setText(transactionId);

                        TextView txAmount = (TextView) dialogVoucher.findViewById(R.id.lblAmount);
                        if (txAmount != null) txAmount.setText(amount);

                        TextView txStatus = (TextView) dialogVoucher.findViewById(R.id.lblStatus);
                        if (txStatus != null) txStatus.setText(status);

                        Button btnClose = (Button) dialogVoucher.findViewById(R.id.btnClose);
                        if (btnClose != null)
                            btnClose.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialogVoucher.dismiss();
                                }
                            });
                        dialogVoucher.show();
                    }
                });
            }

            @Override
            public void onCompleted() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pDialogConfirm.dismiss();
                    }
                });
            }

            @Override
            public void onError() {
                Toast.makeText(MainActivity.this,R.string.msg_errorConfirmPayment,Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Method to confirm the transaction
     */
    public void voidTransaction(String transactionId,View v){
        final ProgressDialog pDialogConfirm = ProgressDialog.show(MainActivity.this,getString(R.string.loading),getString(R.string.voiding_transaction));
        //confirm the payment, here you can make the "buy" method of the web service, that will confirm the sales and will return data of the transaction
        paymentGateway.voidTransaction(transactionId, new PaymentGatewayTest.OnRequestVoidTransaction() {
            @Override
            public void onSuccess(final JSONObject transaction) {
                //<!--{"transactionId":"98bfd8db-3921-47d9-a2e1-58ee171f6a4f","status":"processed","amount":29.99,"createdAt":"2016-10-06T00:03:58.385Z"}-->
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String transactionId = null,status = null,amount = null, dateTransaction = null;
                        try{
                            transactionId = "123123";
                            status = transaction.has("status") ? transaction.getString("status") : null;
                            amount = transaction.has("amount") ? transaction.getString("amount") : null;
                            dateTransaction = transaction.has("createdAt") ? transaction.getString("createdAt") : null;
                        }catch (Exception exc){
                            Log.e(MainActivity.class.getCanonicalName(),"",exc);
                        }

                        final Dialog dialogVoucher = new Dialog(MainActivity.this);
                        dialogVoucher.setTitle(R.string.voided);
                        dialogVoucher.setContentView(R.layout.dg_voucher_payment);
                        dialogVoucher.setCancelable(false);

                        TextView txDate = (TextView) dialogVoucher.findViewById(R.id.lblDate);
                        if (txDate != null) txDate.setText(dateTransaction);

                        TextView txTransaction = (TextView) dialogVoucher.findViewById(R.id.lblTransactionId);
                        if (txTransaction != null) txTransaction.setText(transactionId);

                        TextView txAmount = (TextView) dialogVoucher.findViewById(R.id.lblAmount);
                        if (txAmount != null) txAmount.setText(amount);

                        TextView txStatus = (TextView) dialogVoucher.findViewById(R.id.lblStatus);
                        if (txStatus != null) txStatus.setText(status);

                        Button btnClose = (Button) dialogVoucher.findViewById(R.id.btnClose);
                        if (btnClose != null)
                        btnClose.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialogVoucher.dismiss();
                            }
                        });
                        dialogVoucher.show();
                    }
                });
            }

            @Override
            public void onCompleted() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pDialogConfirm.dismiss();
                    }
                });
            }

            @Override
            public void onError() {
                Toast.makeText(MainActivity.this,R.string.msg_errorConfirmPayment,Toast.LENGTH_LONG).show();
            }
        });
    }
}
